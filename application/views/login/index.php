    <div class="container-fluid login-page">
        <form>
            <div class="row mb-3">
                <div class="col-sm-10">
                    <div class="input-group flex-nowrap">
                        <span class="input-group-text bg-transparent border-white text-white login-logo"><i class="fa-regular fa-user"></i></span>
                        <input type="email" class="form-control border-white bg-transparent py-2 text-white login-input" id="inputEmail3" placeholder="USERNAME">
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-sm-10">
                    <div class="input-group flex-nowrap">
                        <span class="input-group-text bg-transparent border-white text-white login-logo"><i class="fa-solid fa-lock"></i></span>
                        <input type="password" class="form-control border-white bg-transparent py-2 text-white login-input" id="inputPassword3" placeholder="PASSWORD">
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-purple form-control py-2 fw-bold" onclick="location.href='<?php echo base_url(); ?>'">LOGIN</button>
                </div>
            </div>
            <div class="row mb-2 text-end">
                <div class="col-sm-10">
                    <a href="#" class="clear-anchor fw-normal text-white">Forgot password?</a>
                </div>
            </div>
        </form>
    </div>