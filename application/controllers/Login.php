<?php

class Login extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url'); 

        $data['judul'] = 'Halaman Login';
        $data['style'] = 'loginStyle.css';

        $this->load->view('templates/header', $data);
        $this->load->view('login/index');
        $this->load->view('templates/footer');
    }
}